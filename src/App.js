import logo from './logo.svg';
import './App.css';
import Sidebar from "./components/Sidebar";
import NotificationBanner from "./components/NotificationBanner";

function App() {
  return (
      <>
          <Sidebar />
      </>
  );
}

export default App;

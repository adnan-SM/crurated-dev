import {classNames} from "../utils";

export default function CruratedSecondaryTitle({ text, additionalClasses }) {
    return (
        <h3 className={classNames(additionalClasses)}>{text}</h3>
    )
}
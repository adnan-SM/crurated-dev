import React from 'react';
import { useTimer } from 'react-timer-hook';

export default function CountdownTimer({ expiryTimestamp }) {
    const {
        seconds,
        minutes,
        hours,
        days,
        isRunning,
        start,
        pause,
        resume,
        restart,
    } = useTimer({ expiryTimestamp});

    return (
        <div className=' text-left lg:text-center text-[#5F302F]'>
            <div className='flex gap-x-4'>
                <span className='flex flex-col justify-between'>
                    {days}
                    <span>Days</span>
                </span>
                &nbsp;
                <span className='flex flex-col justify-between'>
                    {hours}
                    <span>Hours</span>
                </span>
                &nbsp;
                <span className='flex flex-col justify-between'>
                    {minutes}
                    <span>Mins</span>
                </span>
                &nbsp;
                <span className='flex flex-col justify-between'>
                    {seconds}
                    <span>Secs</span>
                </span>
            </div>
        </div>
    )
}
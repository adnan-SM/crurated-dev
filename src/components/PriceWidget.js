import Dropdown from "./Dropdown";

export default function PriceWidget({ dropdownItems }) {
    return (
        <div className="bg-[#F5EEEB] w-full lg:w-72 h-36 rounded my-8 border-1 border-gray-50 relative">
            <div className="absolute left-0 right-0 mx-2 lg:mx-auto top-0 mt-4 lg:w-[16.5rem] h-24 rounded bg-white">
                <div className="flex flex-row justify-around lg:justify-between items-center">
                    <Dropdown itemList={dropdownItems} />
                    <Dropdown itemList={dropdownItems} />
                </div>
            </div>
        </div>
    )
}
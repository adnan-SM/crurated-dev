export default function CollectionItem({ wineName, year, qty, volume}) {
    return (
        <div className="grid grid-cols-[2fr_1fr_1fr] my-2">
            <div className="text-sm">{wineName}</div>
            <div className="text-sm ml-12">{year}</div>
            <div className="text-sm bg-[#F5EEEB] rounded px-1 text-center">x{qty} of {volume}L</div>
        </div>
    )
}
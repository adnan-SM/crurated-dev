import {classNames} from "../utils";

export default function CruratedHeaderTitle({ text, additionalClasses }) {
    return (
        <h1 className={classNames('text-2xl font-semibold text-gray-900', additionalClasses)}>{text}</h1>
    )
}
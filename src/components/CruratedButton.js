import {classNames} from "../utils";

export default function CruratedButton({ label, additionalClasses, onClickHandler }) {
    return (
        <button
            onClick={onClickHandler}
            className={classNames("bg-[#5F302F] text-white py-2 px-4 rounded h-14 w-full lg:h-16 lg:w-72", additionalClasses)}
        >
            {label}
        </button>
    )
}
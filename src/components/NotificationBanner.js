import { ChevronRightIcon } from '@heroicons/react/outline'

export default function NotificationBanner({ bannerText = 'Set this text via bannerText prop', bannerLogoPath, bannerTextColor = 'text-white' }) {
    return (
        <div className="bg-[#232323] rounded-sm">
            <div className="max-w-7xl mx-auto py-3 px-3 sm:px-6 lg:px-8">
                <div className="flex items-center justify-between flex-wrap">
                    <div className="w-0 flex-1 flex items-center">
            <span className="flex p-2 rounded-lg">
              <img src='/logo.svg' className="h-8 w-8 text-white" aria-hidden="true"  alt={'logo'}/>
            </span>
                        <p className={`ml-3 font-medium ${bannerTextColor} truncate`}>
                            <span className="inline">{bannerText}</span>
                        </p>
                    </div>
                    <ChevronRightIcon className="h-8 w-8 text-white" aria-hidden="true"  alt={'logo'}/>
                </div>
            </div>
        </div>
    )
}
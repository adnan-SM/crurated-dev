import CruratedSecondaryTitle from "./CruratedSecondaryTitle";
import CollectionItem from "./CollectionItem";
import CruratedSecondaryButton from "./CruratedSecondaryButton";
import PriceWidget from "./PriceWidget";
import CruratedButton from "./CruratedButton";
import CruratedHeaderTitle from "./CruratedHeaderTitle";
import CountdownTimer from "./CountdownTimer";

export default function CollectionCard({ sellerTitle, sellerDescription, collectionTitle, expiryTime, collectionDescription }) {
    return (
        <div className="bg-white rounded-sm p-8 lg:p-12">
            <div className="flex flex-col-reverse lg:flex-row justify-between gap-y-4 lg:gap-y-0">
                <CruratedHeaderTitle text={sellerTitle} />
                <CountdownTimer expiryTimestamp={expiryTime} />
            </div>
            <div className="hidden lg:block">
                <CruratedSecondaryTitle text={sellerDescription} />
            </div>
            <div className="flex flex-col md:flex-row justify-between my-4">
                <div className="my-4">
                    <hr className="max-w-lg bg-black h-1 border-black border-2 my-4"/>
                    <CruratedSecondaryTitle additionalClasses="text-[#5F302F] text-sm lg:text-xl" text={collectionTitle} />
                    <hr className="max-w-lg bg-gray-200 h-0.5 my-4"/>
                    <div className="hidden lg:block">
                        <CollectionItem wineName="Borgogno Braolo DOCG Cannubi" year={'2009'} qty={2} volume={0.75} />
                        <CollectionItem wineName="Borgogno Braolo DOCG Cannubi" year={'2009'} qty={2} volume={0.75} />
                        <CollectionItem wineName="Spritzer Blue" year={'2011'} qty={5} volume={1} />
                        <CollectionItem wineName="Jackson White" year={'1987'} qty={4} volume={0.5} />
                        <CollectionItem wineName="Borgogno Braolo DOCG Cannubi" year={'2009'} qty={2} volume={0.75} />
                        <CollectionItem wineName="Jackson White" year={'1987'} qty={4} volume={0.5} />
                    </div>
                </div>
                <div className="flex flex-col items-center justify-end gap-y-1">
                    <CruratedSecondaryButton label="Add a lot" />
                    <CruratedSecondaryButton label="Add a lot" />
                    <CruratedSecondaryButton label="Add a lot" />
                    <CruratedSecondaryButton label="Add a lot" />
                </div>
                <div className="flex flex-col items-center justify-end">
                    <PriceWidget />
                    <CruratedButton label={'Submit an offer'} />
                </div>
            </div>
        </div>
    )
}
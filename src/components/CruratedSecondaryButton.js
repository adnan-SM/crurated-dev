export default function CruratedSecondaryButton({ label, onClickHandler }) {
    return (
        <button onClick={onClickHandler} className="bg-[#F5EEEB] text-[#5F302F80] py-4 px-16 rounded">
            {label}
        </button>
    )
}